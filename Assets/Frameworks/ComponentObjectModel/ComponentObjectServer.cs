﻿using UnityEngine;
using System.Collections;


/// <summary>
/// This has the role of retaining all the components
/// that are then injected into a GameObject and all components
/// in this list will have access to all components in every ComponentObject
/// </summary>
public class ComponentObjectServer : MonoBehaviour
{
    [SerializeField]
    private GameObject[] _compPrefabs;

    private BaseComponentObject[] _compObjects;

    new private Transform transform;

    public bool IsInitialized
    {
        private set;
        get;
    }
	// Use this for initialization
	void Start ()
    {
        IsInitialized = false;
        transform = base.GetComponent<Transform>();
        _compObjects = new BaseComponentObject[_compPrefabs.Length];
        for (int i = 0; i < _compPrefabs.Length; i++)
        {
            var go = GameObject.Instantiate(_compPrefabs[i]);
            go.transform.SetParent(transform);
            go.name = _compPrefabs[i].name;
            _compObjects[i] = go.GetComponent<BaseComponentObject>();
            _compObjects[i].InitBase(this);
        }
        IsInitialized = true;
	}

    public T GetComponentObject<T>()
    {
        for (int i = 0; i < _compObjects.Length; i++)
        {
            var comp = _compObjects[i];
            if (comp is T)
            {
                return (T)System.Convert.ChangeType(comp, typeof(T));
            }
        }

        return default(T);
    }
}
