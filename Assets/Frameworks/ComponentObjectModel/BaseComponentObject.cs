﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BaseComponentObject : MonoBehaviour
{
    private ComponentObjectServer _server;
    private MonoBehaviour[] _components;

    public void InitBase(ComponentObjectServer server)
    {
        _server = server;
    }

    public virtual void Init()
    {
        var components = GetComponents<MonoBehaviour>();
        _components = components;
    }

    private IEnumerator StartDelayed()
    {
        yield return new WaitUntil(() => _server != null && _server.IsInitialized);
        Init();
    }

    /// <summary>
    /// This gets the first component found in any of the other
    /// game objects that are childs of the same parent gameObject
    /// that works as a server
    /// </summary>
    /// <typeparam name="T">A class derived from BaseComponentObject</typeparam>
    /// <returns></returns>
    public T GetComponentObject<T>()
    {
        return _server.GetComponentObject<T>();
    }

    /// <summary>
    /// This gets the first found component only in the current game object
    /// </summary>
    /// <typeparam name="T">A class derived from BaseComponentObject</typeparam>
    /// <returns></returns>
    public T GetPrimitiveComponent<T>()
    {
        var comp = findComponent<T>();
        return comp;
    }

    ///<summary>
    /// This finds the first Monobehaviour component
    /// of the same type T.
    ///</summary>
    ///<returns>A reference of type T</returns>
    private T findComponent<T>()
    {
        T comp = default(T);
        for (int i = 0; i < _components.Length; i++)
        {
            if (_components[i] is T)
            {
                comp = (T)Convert.ChangeType(_components[i], typeof(T));
                break;
            }
        }
        return comp;
    }
#region MonoBehaviour methods
    void Start()
    {
        StartCoroutine(StartDelayed());
    }

    public virtual void Update()
    {
        if (!_server.IsInitialized)
            return;
    }

    public virtual void FixedUpdate()
    {
        if (!_server.IsInitialized)
            return;
    }
#endregion
}
